# frontend_interaction

## Before you build
Copy .env.template and name the file .env! Then, change the IP of the servers to match where you want to run. For example, on a local machine, let `REACT_APP_ROS_WEBBRIDGE_URL=ws://localhost:9090/` but on the rover it will be a different IP.

## To build a static website
1. Simply clone this repo on gitlab. If you have a ROS enabled system, make sure to clone this inside a `catkin_ws`. In the project root dir, do:
2. `npm install`
3. `npm run build`
4. To have the website on a raspberry pi, you can do the above and build on the pi itself if the pi has wifi, or you can zip up the entire folder and scp over

## To run on a ROS enabled system
1. `catkin build frontend_interaction`
2. source your `~/catkin_ws/devel/setup.zsh`
3. `roslaunch frontend_interaction frontend.launch`

## To run on a non-ROS system
1. `cd` into the root dir of this project
2. `./node_modules/.bin/serve -s build`

## To control
* arm
* drivetrain
* science modules

## input from user
* actual controller 
* in browser joystick/keybinds

## communcation
* Websocket to ros-webbridge which converts and broadcasts to ROS messages.
