import mercantile
import requests
import random
import shutil
import os
import time

# Around MDRS and surrounding area
bounds = (
    -116.168664,
    32.741981,
    -116.016599,
    32.854003,
)

# OSM TOS sets a max of 250 tiles that can be downloaded btw
all_tiles = mercantile.tiles(*bounds, zooms=[12, 13, 14, 15, 16])
servers = ["a", "b", "c"]
main_path = "./public/tiles_topo"

all_tiles = list(all_tiles)
print(f"{len(all_tiles)} tiles need to be downloaded.")

for i, t in enumerate(all_tiles):
    print(f"#{i} x: {t.x} y: {t.y} z: {t.z}")
    url = f"https://{random.choice(servers)}.tile.opentopomap.org/{t.z}/{t.x}/{t.y}.png"

    folder_path = f"{main_path}/{t.z}/{t.x}"
    if os.path.exists(f"{folder_path}/{t.y}.png"):
        continue
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    response = requests.get(url, stream=True)

    with open(f"{folder_path}/{t.y}.png", "wb") as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response
    time.sleep(1)
