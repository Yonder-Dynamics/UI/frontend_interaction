import React from "react"
import { Subscriber, getTopic, useRos } from "rosreact"

let subscriberCallbacks = {};

export function registerToSubscriber(topic, callback) {
    if (topic in subscriberCallbacks) {
        subscriberCallbacks[topic].push(callback);
    } else {
        subscriberCallbacks[topic] = [callback]
    }
}

let topicToPublisher = {};

export function writeToPublisher(topic, msg) {
    topicToPublisher[topic].publish(msg);
}

function createSubscriber(topic, messageType, throttleRate = 10) {
    return <Subscriber
        topic={topic}
        messageType={messageType}
        throttleRate={throttleRate}
        customCallback={(msg) => { subscriberCallbacks[topic].map((callback) => callback(msg)) }}
    />
}

function Publisher(topicName, messageType) {
    const topicSettings = {
        topic: topicName,
        messageType: messageType
    };
    let topic = getTopic(useRos(), topicSettings);
    topicToPublisher[topicName] = topic;
}

export default function RosTopics(props) {
    let ros = useRos()
    ros.on('connection', () => {
        props.setRoverConnected(true);
    });
    ros.on('close', () => {
        props.setRoverConnected(false);
    });
    ros.on('error', () => {
        props.setRoverConnected(false);
    });

    return <React.Fragment>
        {createSubscriber(
            "/battery_level",
            "std_msgs/Float64MultiArray",
            100
        )}

        {createSubscriber(
            "/mavros/global_position/global",
            "sensor_msgs/NavSatFix",
            100
        )}

        {createSubscriber(
            "/mavros/global_position/compass_hdg",
            "std_msgs/Float64",
            100
        )}

        {createSubscriber(
            "/state",
            "std_msgs/String"
        )}

        {createSubscriber(
            "/feedback",
            "std_msgs/Float32MultiArray",
            100
        )}

        {createSubscriber(
            "/spectrometer_data",
            "std_msgs/Float64MultiArray",
            100
        )}

        {createSubscriber(
            "/dump_soil_done",
            "std_msgs/Empty"
        )}

        {createSubscriber(
            "/experiment_progress",
            "std_msgs/Float64MultiArray"
        )}

        {createSubscriber(
            "/joint_states",
            "sensor_msgs/JointState",
            100
        )}

        {createSubscriber(
            "/waypoint_list/get",
            "waypoint_storage/MapWaypoints",
            100
        )}

        {Publisher(
            "/elbow_offset_correction",
            "std_msgs/Float64"
        )}

        {Publisher(
            "/rover_waypoint",
            "std_msgs/Float64MultiArray"
        )}

        {Publisher(
            "/drive_train/set_pid",
            "std_msgs/Float64MultiArray"
        )}

        {Publisher(
            "/pid_tuning_values",
            "std_msgs/Float64MultiArray"
        )}

        {Publisher(
            "/zero_encoder",
            "std_msgs/Int16"
        )}

        {Publisher(
            "/set_state",
            "std_msgs/String"
        )}

        {Publisher(
            "/arm_mode",
            "std_msgs/Int16"
        )}

        {Publisher(
            "/execute_profile",
            "std_msgs/Int16"
        )}

        {Publisher(
            "/manual_drive_train",
            "std_msgs/Float64MultiArray"
        )}

        {Publisher(
            "/manual_drive_train/velocity",
            "std_msgs/Float64MultiArray"
        )}

        {Publisher(
            "/arm_speeds",
            "std_msgs/Float64MultiArray"
        )}

        {Publisher(
            "/dump_soil",
            "std_msgs/Int16"
        )}

        {Publisher(
            "/run_experiment",
            "std_msgs/Int16MultiArray"
        )}

        {Publisher(
            "/waypoint_list/put",
            "waypoint_storage/MapWaypoints"
        )}

        {Publisher(
            "/waypoint_list/request",
            "std_msgs/Int16"
        )}
    </React.Fragment>
}
