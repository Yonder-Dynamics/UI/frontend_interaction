import React, { useState } from "react";
import {
  Button, Form, FormGroup, Label, Row, Col,
  Input,
} from "reactstrap";

const PIDTuner = (props) => {
    const [ pidValues, setPidValues ] = useState({"P": 0, "I": 0, "D": 0, })
    return (
          <Form>
            <Row className="row-cols-lg-auto g-3 align-items-center">
              <Label style={{ marginLeft: 30 }}> {props.tuningPID} </Label>
              {["P", "I", "D"].map((term) => {
                return <Col key={term}>
                  <FormGroup>
                    <Label
                      for={term}
                    >
                      {term}
                      <Input
                        id={term}
                        value={pidValues[term]}
                        onChange={e => {
                          let newPIDValues = { ...pidValues }
                          newPIDValues[term] = e.target.value;
                          setPidValues(newPIDValues)
                        }}
                      />
                    </Label>
                  </FormGroup>
                </Col>
              })}
              <Col>
                <Button onClick={() => props.onSubmit(pidValues)} style={{ marginRight: 20 }} type='button'>
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>
    )
}

export default PIDTuner
