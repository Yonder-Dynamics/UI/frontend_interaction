import React from "react";
import {
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Area,
  ResponsiveContainer,
} from "recharts";
import {
  Button,
  Container,
  Row,
  Col,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { registerToSubscriber } from "../RosTopics";

const MIN_X_VALUE = 350
const MAX_X_VALUE = 700
// interval between ticks
const TICK_INTERVAL = 50

const numToExperiment = {
  0: "Raman",
  1: "Dapi",
  2: "Nile Red",
  3: "pH",
  4: "Dark Background",
  5: "Light Background"
}

export default class Spectroscopy extends React.Component {
  state = {
    exprAmplitudes: {},
    exprSelected: null,
    sampleSelected: null,
    wavelengths: new Array(2048).fill(0),
    integrationTime: 50,
    averages: 1,
  };

  componentDidMount = () => {
    registerToSubscriber("/spectrometer_data",
      (msg) => {
        let temp = this.state.exprAmplitudes;
        let key = [msg.data[0], msg.data[1]]
        temp[key] = msg.data.slice(2)
        this.setState({ exprAmplitudes: temp })
      });

    let wavelengths = new Array(2048).fill(0);
    for (let i = 0; i < 2048; i++) {
      const c0 = 717.783
      const c1 = -0.179129;
      const c2 = -0.00000383008;
      const c3 = 0;
      let wavelength = c0 + c1 * i + c2 * i * i + c3 * i * i * i;
      wavelengths[i] = Math.round(wavelength * 1000) / 1000;
    }

    this.setState({ wavelengths: wavelengths });
  };

  render() {
    let samples = {}
    for (const [key, value] of Object.entries(this.state.exprAmplitudes)) {
      let arr = key.split(",")
      if (arr[0] in samples) {
        samples[arr[0]].push(arr[1]);
      } else {
        samples[arr[0]] = [arr[1]]
      }
    }

    let data = new Array(2048);
    let toUse = this.state.exprAmplitudes[[this.state.sampleSelected, this.state.exprSelected]]

    for (let i = 0; i < 2048; i++) {
      if (toUse === undefined) {
        data[i] = {
          wavelength: this.state.wavelengths[i],
          amplitude: i,
        };
      } else {
        data[i] = {
          wavelength: this.state.wavelengths[i],
          amplitude: toUse[i],
        };
      }
    }

    return (
      <div>
        <Container style={{ padding: 20 }}>
          <Row>
            <Col>
              <Dropdown
                isOpen={this.state.sampleDropOpen}
                toggle={(x) =>
                  this.setState((state) => ({
                    sampleDropOpen: !state.sampleDropOpen,
                  }))
                }
              >
                <DropdownToggle caret>
                  {`Sample Number: ${parseInt(this.state.sampleSelected) + 1}`}
                </DropdownToggle>
                <DropdownMenu>
                  {
                    Object.keys(samples).length === 0 ?
                      [<DropdownItem> No Data Recorded </DropdownItem>]
                      :
                      Object.keys(samples).map((sample) => {
                        return (
                          <DropdownItem key={sample} onClick={() => this.setState({ sampleSelected: sample })}>
                            {parseInt(sample) + 1}
                          </DropdownItem>
                        );
                      })}
                </DropdownMenu>

              </Dropdown>
            </Col>
            <Col>
              <Dropdown
                isOpen={this.state.exprDropOpen}
                toggle={(x) =>
                  this.setState((state) => ({
                    exprDropOpen: !state.exprDropOpen,
                  }))
                }
              >
                <DropdownToggle caret>
                  {`Experiment Type: ${numToExperiment[this.state.exprSelected]}`}
                </DropdownToggle>
                <DropdownMenu>
                  {
                    this.state.sampleSelected === null ?
                      [<DropdownItem> No Data Recorded </DropdownItem>]
                      :
                      samples[this.state.sampleSelected].map((expr) => {
                        return (
                          <DropdownItem key={expr} onClick={() => this.setState({ exprSelected: expr })}>
                            {numToExperiment[expr]}
                          </DropdownItem>
                        );
                      })}
                </DropdownMenu>
              </Dropdown>
            </Col>
          </Row>
        </Container>
        <ResponsiveContainer
          height={600}
        >
          <AreaChart
            data={data}
            margin={{ top: 30, right: 60, left: 30, bottom: 30 }}
          >
            <defs>
              <linearGradient id="colorId" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
              </linearGradient>
            </defs>
            <XAxis
              dataKey="wavelength"
              type="number"
              domain={[MIN_X_VALUE, MAX_X_VALUE]}
              tickCount={((MAX_X_VALUE - MIN_X_VALUE) / TICK_INTERVAL) + 1}
              allowDataOverflow={true}
            />
            <YAxis type="number" domain={['dataMin', 'dataMax']} tickFormatter={(value) => Math.round(value, 3)} />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip formatter={(value) => Math.round(value, 3)} />
            <Area
              type="monotone"
              dataKey="amplitude"
              stroke="#8884d8"
              fillOpacity={1}
              fill="url(#colorId)"
            />
          </AreaChart>
        </ResponsiveContainer>
      </div >
    );
  }
}
