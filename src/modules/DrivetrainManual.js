import React from "react";
import {
  Button, ButtonGroup, Form, FormGroup, Label, Row, Col,
  Input, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Dropdown,
  Container,
} from "reactstrap";
import Joystick from "react-joystick";

import { registerToSubscriber, writeToPublisher } from "../RosTopics";
import PIDTuner from "./PIDTuner.js";
import DriveTrainPIDTuner from "./DriveTrainPIDTuner.js";

const joyOptions = {
  mode: "static",
  color: "white",
  position: {
    top: "50%",
    left: "50%",
  },
};

const containerStyle = {
  height: "200px",
  width: "100%",
  background: "linear-gradient(to right, #E684AE, #79CBCA, #77A1D3)",
  borderTopLeftRadius: "10px",
  borderTopRightRadius: "10px",
  overflow: "hidden",
  position: "relative",
  zIndex: 1,
};

const autonomous = "autonomous";
const manual = "manual";
const off = "off";
const goal_reached = "goal_reached";

const drivetrain = 0;
const armScience = 1;

const droop = "droop";
const inverse = "inverse";
const close_loop = "close_loop";
const profile = "profile";
const direct = "direct"

const armStateToNum = { "off": 0, "droop": 1, "close_loop": 2, "profile": 3, "inverse": 4, "direct": 5 }

const profile1 = "home_stow"
const profile2 = "operation"
const profile3 = "grab_soil"
const profile4 = "science_box"

const profileToNum = { "home_stow": 1, "operation": 2, "grab_soil": 3, "science_box": 4 }

const jointToNum = { "Base": 0, "Shoulder": 1, "Elbow": 2 }


export default class DrivetrainManual extends React.Component {
  state = {
    x: 0,
    y: 0,
    angle: null,
    driveToggle: drivetrain,
    armControlState: off,
    profileState: off,
    tuningPID: null,
    pidValues: { "P": "", "I": "", "D": "" },
    roverState: off,
    showArmOffsetInstructions: false,
  };

  constructor(props) {
    super(props);
    this.setState({ keyboardControl: props.keyboardControlRef,
      });

    registerToSubscriber("/state",
      (msg) => this.setState({ roverState: msg.data }));
  }

  update = (angle, force) => {
    let x = Math.cos(angle) * force;
    let y = Math.sin(angle) * force;
    this.setState({ x, y, angle });
  };

  managerListener = (manager) => {
    manager.on("move", (_, stick) =>
      this.update(stick.angle.radian, Math.min(1, stick.force))
    );
    manager.on("end", () => this.update(0, 0));
  };

  componentDidMount() {
    setInterval(() => {
      if (this.state !== null && this.props.onMove != null) {
        let { x, y, angle } = this.state;
        this.props.onMove(x, y, angle);
      }
    }, 16);
  }

  setDrivetrain = () => {
    this.setState({ driveToggle: drivetrain, tuningPID: null }, () => {
      if (this.state.roverState === off) return;
      this.props.setKeyboardControlState(false);
      this.props.setJoystickReaderState(true);
    });
  };

  setArmScience = () => {
    this.setState({ driveToggle: armScience }, () => {
      if (this.state.roverState === off) return;
      this.props.setKeyboardControlState(true);
      this.props.setJoystickReaderState(false);
    });
  };

  setArmControlState = (state) => {
    this.setState({ armControlState: state });
    writeToPublisher("/arm_mode", { data: armStateToNum[state] });
  };

  setArmProfileState = (state) => {
    this.setState({ profileState: state });
  };

  executeArmProfile = () => {
    writeToPublisher("/execute_profile", { data: profileToNum[this.state.profileState] });
  };

  setOff = () => {
    this.setState({ roverState: off });
    writeToPublisher("/set_state", { data: off });
    this.props.setKeyboardControlState(false);
    this.props.setJoystickReaderState(false);
  };

  setManual = () => {
    this.setState({ roverState: manual });
    writeToPublisher("/set_state", { data: manual });
    if (this.state.driveToggle === drivetrain) {
      this.setDrivetrain();
    } else if (this.state.driveToggle === armScience) {
      this.setArmScience();
    }
  };

  setAutonomous = () => {
    this.setState({ roverState: autonomous });
    writeToPublisher("/set_state", { data: autonomous });
    this.props.setKeyboardControlState(false);
    this.props.setJoystickReaderState(false);
  };

  sendArmPIDData = (values) => {
    // don't send data unless all fields are filled in floats
    for (const prop in values) {
      if (isNaN(parseFloat(values[prop]))) return;
    }

    let dataSend = [
      jointToNum[this.state.tuningPID],
      parseFloat(values["P"]),
      parseFloat(values["I"]),
      parseFloat(values["D"])
    ]

    writeToPublisher("/pid_tuning_values", { layout: { dim: [], data_offset: 0 }, data: dataSend });
  }

  sendZeroEncoder = (joint) => {
    writeToPublisher("/zero_encoder", { data: jointToNum[joint] });
  }

  offsetElbow = (offset) => {
    writeToPublisher("/elbow_offset_correction", { data: offset });
  }

  toggleArmOffsetInstructions = () => {
    this.setState({ showArmOffsetInstructions: !this.state.showArmOffsetInstructions})
  }


  render() {
    return (
      <div>
        <Joystick
          options={joyOptions}
          containerStyle={containerStyle}
          managerListener={this.managerListener}
        />
        <p style={{ fontWeight: "bold", color: this.state.roverState === goal_reached ? "green" : "red", textAlign: "center" }}>
          {this.state.roverState}
        </p>
        <ButtonGroup style={{ margin: 10, display: "flex" }}>
          <Button
            color={this.state.roverState === manual ? "primary" : "secondary"}
            onClick={this.setManual}
            active={this.state.roverState === manual}
          >
            Manual
          </Button>
          <Button
            color={
              this.state.roverState === autonomous ? "primary" : "secondary"
            }
            onClick={this.setAutonomous}
            active={this.state.roverState === autonomous}
          >
            Autonomous
          </Button>
          <Button
            color={this.state.roverState === off ? "primary" : "secondary"}
            onClick={this.setOff}
            active={this.state.roverState === off}
          >
            Off
          </Button>
        </ButtonGroup>
        <ButtonGroup style={{ margin: 10, display: "flex" }}>
          <Button
            color={
              this.state.driveToggle === drivetrain ? "primary" : "secondary"
            }
            onClick={this.setDrivetrain}
            active={this.state.driveToggle === drivetrain}
          >
            Drivetrain
          </Button>
          <Button
            color={
              this.state.driveToggle === armScience ? "primary" : "secondary"
            }
            onClick={this.setArmScience}
            active={this.state.driveToggle === armScience}
          >
            Arm/Science
          </Button>
        </ButtonGroup>
        {this.state.driveToggle === drivetrain && (
          <DriveTrainPIDTuner />
         )}
        {this.state.driveToggle === armScience && (
          <Container>
            <Row>
              <Col>
                <Dropdown
                  isOpen={this.state.controlState}
                  toggle={(x) =>
                    this.setState((state) => ({
                      controlState: !state.controlState,
                    }))
                  }
                >
                  {this.state.driveToggle === armScience && (
                    <DropdownToggle caret> Control State: {this.state.armControlState} </DropdownToggle>
                  )}
                  {this.state.driveToggle === armScience && this.state.controlState && (
                    <DropdownMenu>
                      {[
                        [off, "Off"],
                        [droop, "Droop"],
                        [close_loop, "Closed Loop"],
                        [profile, "Profile"],
                        [inverse, "Inverse Kinematics"],
			[direct, "Direct"]
                      ].map(([state, buttonName]) => {
                          return (
                            <DropdownItem key={buttonName}
                              color={
                                this.state.armControlState === state
                                  ? "primary"
                                  : "secondary"
                              }
                              onClick={() => this.setArmControlState(state)}
                              active={this.state.armControlState === state}
                            >
                              {buttonName}
                            </DropdownItem>
                          );
                        })}
                    </DropdownMenu>
                  )}
                </Dropdown>
                <Dropdown
                  isOpen={this.state.controlProfile}
                  toggle={(x) =>
                    this.setState((state) => ({
                      controlProfile: !state.controlProfile,
                    }))
                  }
                >
                  {this.state.armControlState === profile && this.state.driveToggle === armScience &&
                    <>{this.state.profileState === profile1 ?
                      <DropdownToggle caret style={{ margin: 5 }}> {profile1} </DropdownToggle>
                      :
                      <>{this.state.profileState === profile2 ?
                        <DropdownToggle caret style={{ margin: 5 }}> {profile2} </DropdownToggle>
                        :
                        <>{this.state.profileState === profile3 ?
                          <DropdownToggle caret style={{ margin: 5 }}> {profile3} </DropdownToggle>
                          :
                          <>{this.state.profileState === profile4 ?
                            <DropdownToggle caret style={{ margin: 5 }}> {profile4} </DropdownToggle>
                            :
                            <>{this.state.profileState === profile3 ?
                              <DropdownToggle caret style={{ margin: 5 }}> Profile State: 3 </DropdownToggle>
                              :
                              <DropdownToggle caret style={{ margin: 5 }}> Choose Profile State </DropdownToggle>
                            }
                            </>
                          }
                          </>
                        }
                        </>
                      }
                      </>
                    }
                    </>
                  }
                  {this.state.driveToggle === armScience && this.state.armControlState === profile && this.state.controlProfile && (
                    <DropdownMenu style={{ margin: 5 }}>
                      {[
                        [profile1, "home_stow"],
                        [profile2, "operation"],
                        [profile3, "grab_soil"],
                        [profile4, "science_box"],
                      ].map(([state, buttonName]) => {
                          return (
                            <DropdownItem key={buttonName}
                              color={
                                this.state.armControlState === state
                                  ? "primary"
                                  : "secondary"
                              }
                              onClick={() => this.setArmProfileState(state)}
                              active={this.state.profileState === state}
                            >
                              {buttonName}
                            </DropdownItem>
                          );
                        })}
                    </DropdownMenu>
                  )}
                </Dropdown>
                {this.state.driveToggle === armScience && this.state.armControlState === profile && this.state.profileState !== off && (
                  <Button style={{ margin: 5 }} onClick={this.executeArmProfile}>
                    Execute
                  </Button>
                )}
              {/* </Col> */}
              {/* <Col> */}
                <ButtonGroup style={{ margin: 10, display: "flex" }}>
                  <UncontrolledDropdown>
                    <DropdownToggle caret>
                      Tune PID
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem header>
                        Joint
                      </DropdownItem>
                      {["Base", "Shoulder", "Elbow"].map((joint) => {
                        return <DropdownItem key={joint} onClick={() => this.setState({ tuningPID: joint })}>
                          {joint}
                        </DropdownItem>
                      })}
                    </DropdownMenu>
                  </UncontrolledDropdown>
                  <UncontrolledDropdown>
                    <DropdownToggle caret>
                      Zero Encoder
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem header>
                        Joint
                      </DropdownItem>
                      {["Base", "Shoulder", "Elbow"].map((joint) => {
                        return <DropdownItem key={joint} onClick={() => this.sendZeroEncoder(joint)}>
                          {joint}
                        </DropdownItem>
                      })}
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </ButtonGroup>
              </Col>
              <Col>
                <Button onClick={this.toggleArmOffsetInstructions}>
                  Click for instructions for offsetting elbow encoder
                </Button>
                {this.state.showArmOffsetInstructions && 
                  <div>
                    * Move arm straight up irl
                    <br/>
                    * If in arm bent right in arm viz, add 90
                    <br/>
                    * If arm bent left in arm viz, subtract 90
                  </div>
                }
                <div />
                <Button onClick={() => this.offsetElbow(-90)}>
                  -90 deg elbow offset
                </Button>
                <Button onClick={() => this.offsetElbow(90)}>
                  +90 deg elbow offset
                </Button>
              </Col>
            </Row>
          </Container>
        )}

        {this.state.driveToggle === armScience && this.state.tuningPID && (
          <>
          <PIDTuner label={this.state.tuningPID} onSubmit={this.state.sendArmPIDData}/>
          </>
        )}
      </div>
    );
  }
}
