import React, { useEffect, useState } from "react";
import { Stage, Layer, Text, Image, Line, Circle, Rect } from 'react-konva';
import { registerToSubscriber } from "../RosTopics";
import roverImageSrc from '../images/roverclipart.png';
import compassTemplateSrc from '../images/compass_template.png'; //template from https://www.youtube.com/watch?v=aMEHOU6xpWA

export default function CameraCompass(props) {
    const [compassVal, setCompassVal] = useState(0);
    const [roverImage, setRoverImage] = useState(null);
    const [compassTemplate, setCompassTemplate] = useState(null);
    const [roverLong, setRoverLong] = useState(0);
    const [roverLat, setRoverLat] = useState(0);
    const [waypointList, setWaypointList] = useState(0);
    const [waypointList2, setWaypointList2] = useState(0);

    useEffect(() => {
        registerToSubscriber("/mavros/global_position/compass_hdg",
            (msg) => setCompassVal(msg.data)
        );

        registerToSubscriber("/mavros/global_position/global", (msg) => {
            setRoverLong(msg.longitude);
            setRoverLat(msg.latitude);
        }
        );

        registerToSubscriber("/waypoint_list/get",
            (msg) => setWaypointList(msg.waypoints)
        );

        registerToSubscriber("/waypoint_list/get",
            (msg) => setWaypointList2(msg.waypoints)
        );

        const loadImages = () => {
            const loadRoverImage = new window.Image();
            loadRoverImage.src = roverImageSrc;
            loadRoverImage.onload = () => setRoverImage(loadRoverImage);

            const loadCompassTemplate = new window.Image();
            loadCompassTemplate.src = compassTemplateSrc;
            loadCompassTemplate.onload = () => setCompassTemplate(loadCompassTemplate);
        };

        loadImages();
    }, []);

    // Assuming the compass image's width is the same as the stage's width
    const stageWidth = window.innerWidth * .4;
    const stageHeight = parseInt(stageWidth / props.ratio);
    const normalizedCompassVal = normalizeAngle(compassVal);
    const compassOffset = normalizedCompassVal * (stageWidth / 360);

    return (
        <div style={{ overflow: 'hidden' }}>
            <Stage width={stageWidth} height={stageHeight}>
                <Layer>
                    <Rect
                        y={0}
                        width={stageWidth}
                        fill={"black"}
                        height={115}
                        opacity={0.5}
                    />

                    {compassTemplate && ( //Compass Template image
                        <Image
                            x={-compassOffset}
                            y={0}
                            image={compassTemplate}
                            width={stageWidth}
                            height={compassTemplate.height}
                        />
                    )}
                    {compassTemplate && (
                        <Image
                            x={-compassOffset + stageWidth} //Following Compass Template image
                            y={0}
                            image={compassTemplate}
                            width={stageWidth}
                            height={compassTemplate.height}
                        />
                    )}


                    {/*{roverImage && ( //Static Rover Image
                        <Image
                            x={stageWidth / 2 - roverImage.width / 2}
                            y={300 - roverImage.height / 2}
                            image={roverImage}
                        />
                    )}*/}

                    {Object.values(waypointList).map((waypoint) => {
                        let compassWayPoint = calculateBearing(roverLat, roverLong, waypoint.lat, waypoint.lng);
                        return <Circle
                            x={parseInt(((compassWayPoint / 360) * stageWidth) - compassOffset)} // x-coordinate of the circle
                            y={27} // radius of the circle
                            radius={10}
                            fill={waypoint.color} // fill color of the circle
                        />

                    })}

                    {Object.values(waypointList2).map((waypoint2) => {
                        console.log(waypointList2);
                        let compassWayPoint2 = calculateBearing(roverLat, roverLong, waypoint2.lat, waypoint2.lng);
                        return <Circle
                            x={parseInt((((compassWayPoint2 / 360) * stageWidth) - compassOffset) + stageWidth)} // x-coordinate of the circle
                            y={27} // radius of the circle
                            radius={10}
                            fill={waypoint2.color} // fill color of the circle
                        />

                    })}


                    <Text fontSize={30} text={`${compassVal}°`} //Blue Center Compass Value
                        wrap="char"
                        align="center"
                        x={0}
                        y={80} // Position at the bottom
                        width={stageWidth}
                        fill="cyan" />
                </Layer>
            </Stage>
        </div>
    );
}

function normalizeAngle(angle) {
    while (angle < 0) {
        angle += 360;
    }
    return angle % 360;
}

// latitudeA and longitudeA is the rover's lat and long, other is the waypoint's lat and long
function calculateBearing(latitudeA, longitudeA, latitudeB, longitudeB) {
    const thetaA = latitudeA * (Math.PI / 180);
    const thetaB = latitudeB * (Math.PI / 180);
    const deltaL = (longitudeB - longitudeA) * (Math.PI / 180);

    const X = Math.cos(thetaB) * Math.sin(deltaL);
    const Y = Math.cos(thetaA) * Math.sin(thetaB) - Math.sin(thetaA) * Math.cos(thetaB) * Math.cos(deltaL);

    const beta = Math.atan2(X, Y);

    let roundedPositiveBearing = (beta * 180) / Math.PI
    if (roundedPositiveBearing < 0) {
        roundedPositiveBearing += 360;
    }
    roundedPositiveBearing = Math.round(roundedPositiveBearing)

    if (roundedPositiveBearing > 180) {
        roundedPositiveBearing = roundedPositiveBearing % 180
    }
    else {
        roundedPositiveBearing += 180
    }

    return roundedPositiveBearing;
}