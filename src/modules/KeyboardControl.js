import React from "react";
import { Container, Row, Col } from "reactstrap";
import { writeToPublisher } from "../RosTopics";

const gamma = 2.0;

let keyActions = {
  a: state => {
    state.x_speed += 0.1;
  },
  d: state => {
    state.x_speed -= 0.1;
  },
  w: state => {
    state.y_speed += 0.1;
  },
  s: state => {
    state.y_speed -= 0.1;
  },
  i: state => {
    state.z_speed += 0.1;
  },
  k: state => {
    state.z_speed -= 0.1;
  },
  o: state => {
    state.wrist_up += 0.1;
  },
  l: state => {
    state.wrist_up -= 0.1;
  },
  q: state => {
    state.wrist_angle += 0.1;
  },
  e: state => {
    state.wrist_angle -= 0.1;
  }
};

let buttonMap = {
  Cross: 0,
  Circle: 1,
  Square: 2,
  Triangle: 3,
  L1: 4,
  R1: 5,
  L2: 6,
  R2: 7,
  Share: 8,
  Options: 9,
  L3: 10,
  R3: 11,
  Up: 12,
  Down: 13,
  Left: 14,
  Right: 15,
  PSButton: 16,
  Touchpad: 17
};

const deadzone = 0.16;
const base_scale = 65;
const shoulder_scale = -30;
const elbow_scale = -30;
const wrist_scale = 35;
const wrist_grav = 1.4;
const servo_scale = 1;

export const keysHandled = Object.keys(keyActions);

export default class KeyboardControl extends React.Component {
  state = {
    keysPressed: new Set(),
    x_speed: 0,
    y_speed: 0,
    z_speed: 0,
    wrist_up: 0,
    wrist_angle: 0,
    servo: 0,
    rst_hand: 0,
    shouldSubmit: false
  };

  onKeyDown = e => {
    e.persist();
    this.setState(state => {
      let { keysPressed } = state;
      keysPressed.add(e.key.toLowerCase());
      return keysPressed;
    });
  };

  onKeyUp = e => {
    e.persist();
    this.setState(state => {
      let { keysPressed } = state;
      keysPressed.delete(e.key.toLowerCase());
      return keysPressed;
    });
  };

  update = () => {
    this.setState(
      state => {
        for (const [key, value] of Object.entries(keyActions)) {
          if (state.keysPressed.has(key)) value(state);
        }
      },
      () => {
        if (this.state.shouldSubmit) {
          let dataSend = [
            parseFloat(this.state["x_speed"]),
            parseFloat(this.state["z_speed"]),
            parseFloat(this.state["y_speed"]),
            parseFloat(this.state["wrist_up"]),
            parseFloat(this.state["wrist_angle"]),
            parseFloat(this.state["servo"]),
            parseFloat(this.state["rst_hand"])
          ];
          writeToPublisher("/arm_speeds", { layout: { dim: [], data_offset: 0 }, data: dataSend });
        }
      }
    );
  };

  pollGamepad = gamepad => {
    if (gamepad === undefined || !this.state.shouldSubmit) return;

    let axes = gamepad.axes;
    let lerp = (x, y, alpha) => {
      return (1 - alpha) * x + alpha * y;
    };

    let speeds = axes
      .map((_, idx) => (idx % 2 === 0 ? 1 : -1) * Math.sign(axes[idx]))
      .map((val, idx) => (Math.abs(val * axes[idx]) < deadzone ? 0 : val))
      .map((val, idx) => val * lerp(deadzone, 1, Math.abs(axes[idx])) ** 2);

    let { buttons } = gamepad;
    this.setState(state => {
      let {
        x_speed,
        y_speed,
        z_speed,
        wrist_up,
        wrist_angle,
        servo
      } = state;

      x_speed = speeds[0] * base_scale; // left left-right stick
      y_speed = speeds[3] * elbow_scale; // right up-down stick
      z_speed = speeds[1] * shoulder_scale; // left up-down
      const twistSign = Math.sign(speeds[2]);
      const wrist_twist_speed = - (twistSign * (speeds[2] ** gamma)) * wrist_scale;
      const wrist_rotate_speed = (buttons[buttonMap.R2].value ** gamma - buttons[buttonMap.L2].value ** gamma) * wrist_scale;

      wrist_up = 0;
      wrist_angle = 0;

      wrist_up = wrist_twist_speed + wrist_rotate_speed;
      wrist_angle = wrist_twist_speed - wrist_rotate_speed;

      servo = (buttons[buttonMap.R1].value - buttons[buttonMap.L1].value) * servo_scale;
      servo = Math.min(servo, 255);
      servo = Math.max(servo, -255);

      return {
        x_speed,
        y_speed,
        z_speed,
        wrist_up,
        wrist_angle,
        servo,
        rst_hand: buttons[buttonMap.Cross].pressed ? 1 : 0
      };
    });
  };

  componentDidMount() {
    setInterval(this.update, 60);
  }

  render() {
    let pressed = [];
    let {
      keysPressed,
      x_speed,
      y_speed,
      z_speed,
      wrist_up,
      wrist_angle,
      servo
    } = this.state;

    for (let key of Object.keys(keyActions)) {
      if (keysPressed.has(key)) pressed.push(key);
    }

    return (
      <Container>
        <Row>
          <Col>
            <h4 className="mt-4">Keys Pressed</h4>
            <ul>
              {pressed.map(value => (
                <li>{value}</li>
              ))}
            </ul>
          </Col>
          <Col>
            <h4 className="mt-4">Current Arm Position</h4>
            <ul>
              <li>
                <b>x_speed (a,d):</b> {x_speed.toFixed(2)}
              </li>
              <li>
                <b>y_speed (w,s):</b> {y_speed.toFixed(2)}
              </li>
              <li>
                <b>z_speed (i,k):</b> {z_speed.toFixed(2)}
              </li>
              <li>
                <b>wrist_up (o,l):</b> {wrist_up.toFixed(2)}
              </li>
              <li>
                <b>wrist_angle (q,e):</b> {wrist_angle.toFixed(2)}
              </li>
              <li>
                <b>servo (yeet):</b> {servo.toFixed(2)}
              </li>
            </ul>
          </Col>
        </Row>
      </Container>
    );
  }
}
