import React from "react";
import { Container, Row, Col, ButtonGroup, Button, FormGroup, Label, Input } from "reactstrap";
import { registerToSubscriber, writeToPublisher } from "../RosTopics";

const joystickBoxStyle = {
  display: "table",
  margin: "0 auto"
};

const joystickBackgroundStyle = {
  backgroundColor: "grey",
  width: "125px",
  height: "125px",
  borderRadius: "15%",
  position: "relative"
};

const joystickMarkerBoxStyle = {
  position: "absolute",
  right: "0",
  bottom: "0",
  width: "75px",
  height: "75px"
};

function joystickMarkerStyle(top, left) {
  return {
    position: "absolute",
    top: (-top * 50).toString() + "px",
    left: (left * 50).toString() + "px",
    backgroundColor: "red",
    width: "25px",
    height: "25px",
    borderRadius: "50%",
    border: "3px solid pink"
  };
}

function JoystickElement(props) {
  return (
    <div style={joystickBoxStyle}>
      <div style={joystickBackgroundStyle}>
        <div style={joystickMarkerBoxStyle}>
          <div style={joystickMarkerStyle(props.joy.top, props.joy.left)} />
        </div>
      </div>
    </div>
  );
}

const DriveControlModes = {
  dutyCycle: "dutyCycle",
  velocity: "velocity",
}

const DriveControlSchemes = {
  singleStick: "singleStick",
  splitStick: "splitStick",
  tank: "tank",
  curve: "curve"
}

const clamp = (num, min, max) => {
  return Math.min(Math.max(num, min), max);
};

let joyToSingleStick = (x, y) => {
  let left = 0;
  let right = 0;

  let squareSum = Math.pow(x, 2) + Math.pow(y, 2);
  if (squareSum !== 0) {
    let magnitude = Math.max((Math.abs(y) + Math.abs(x)) / squareSum, 1e-9)
    left = (y + x) / magnitude
    right = (y - x) / magnitude
  }
  console.log("left: " + left + " right: " + right);
  return [left, right]
}

let joyToSplitStick = (x, y) => {
  // let left = 0;
  // let right = 0;

  // left = clamp(y + x, -1, 1)
  // right = clamp(y - x, -1, 1)
  // console.log("left: " + left + " right: " + right);
  // return [left, right]
  let left = 0;
  let right = 0;

  let squareSum = Math.pow(x, 2) + Math.pow(y, 2);
  if (squareSum !== 0) {
    let magnitude = Math.max((Math.abs(y) + Math.abs(x)) / squareSum, 1e-9)
    left = (y + x) / magnitude
    right = (y - x) / magnitude
  }
  left = clamp(left, -1, 1)
  right = clamp(right, -1, 1)
  console.log("left: " + left + " right: " + right);
  return [left, right]

}

let joyToCurve = (x,y) => {
  let ROBOT_WIDTH = 1;
  let CURVE_THREASHHOLD = 0.1;
  let MIN_TURN_R = 1;
  let TURN_INTENSITY = 1; // Must be odd integer !!!
  let speedRatio = 1;
  if (Math.abs(y) > CURVE_THREASHHOLD) {
    let turnR = MIN_TURN_R/Math.pow(y,TURN_INTENSITY);
    speedRatio = (2*turnR - ROBOT_WIDTH)/(2*turnR + ROBOT_WIDTH);
  }
  let left = x * (1/speedRatio);
  let right = x * speedRatio;

  let scaleFactor = Math.max(Math.abs(left), Math.abs(right), 1);

  return [left / scaleFactor, right / scaleFactor]
}

const joyToTank = (y_1, y_2) => {
  return [y_1, y_2];
}

const deadzone = 0.05;

export default class JoystickReader extends React.Component {
  state = {
    gamepad: null,
    gamepadInterval: null,
    leftJoystick: {
      top: 0,
      left: 0
    },
    rightJoystick: {
      top: 0,
      left: 0
    },
    shouldSubmit: false,
    gamma: 2,
    maxSpeed: 1,
    fingerStalled: false,
    driveControlMode: DriveControlModes.velocity,
    driveControlScheme: DriveControlSchemes.singleStick,
  };

  setDriveControlScheme = (controlScheme) => {
    this.setState({driveControlScheme: controlScheme}, () => {
    });
  };

  setDriveControlMode = (controlMode) => {
    this.setState({driveControlMode: controlMode}, () => {
    });
  };


  componentDidMount() {
    registerToSubscriber("/feedback",
      (msg) => this.setState({ fingerStalled: msg.data[msg.data.length - 1] }));

    setInterval(() => {
      let xLeft = this.state.leftJoystick.left;
      let yLeft = this.state.leftJoystick.top;
      let xRight = this.state.rightJoystick.left;
      let yRight = this.state.rightJoystick.top;

      if (this.state.shouldSubmit) {
        let [left, right] = [0, 0];
        switch (this.state.driveControlScheme) {
          case DriveControlSchemes.singleStick:
            [left, right] = joyToSingleStick(xLeft, yLeft)
            break;
          case DriveControlSchemes.splitStick:
            [left, right] = joyToSplitStick(xRight, yLeft);
            break;
          case DriveControlSchemes.tank:
            [left, right] = joyToTank(yLeft, yRight);
            break;
          case DriveControlSchemes.curve:
            [left, right] = joyToCurve(yLeft,xRight);
            break;
          default:
            console.log("unknown drive control scheme: " + this.state.driveControlScheme);
        }

        switch (this.state.driveControlMode) {
          case DriveControlModes.dutyCycle:
            const maxVal = 255;
            left = Math.round(maxVal * left)
            right = Math.round(maxVal * right)
            writeToPublisher("/manual_drive_train", { layout: { dim: [], data_offset: 0 }, data: [left, right] });
            break;
          case DriveControlModes.velocity:
            const maxVelocity = 4;
            left = maxVelocity * left
            right = maxVelocity * right
            writeToPublisher("/manual_drive_train/velocity", { layout: { dim: [], data_offset: 0 }, data: [left, right] });
            break;
          default:
            console.log("unkown drive control mode: " + this.state.driveControlMode);
        }
      }
    }, 50);

    window.addEventListener("gamepadconnected", e => {
      let gamepadInterval = setInterval(this.pollGamepad, 16);
      this.setState({ gamepad: e.gamepad, gamepadInterval });
    });

    window.addEventListener("gamepaddisconnected", () => {
      clearInterval(this.state.gamepadInterval);
      this.setState({
        gamepad: null,
        gamepadInterval: null,
        leftJoystick: {
          top: 0,
          left: 0
        },
        rightJoystick: {
          top: 0,
          left: 0
        }
      });
    });
  }

  pollGamepad = () => {
    let gamepad = navigator.getGamepads()[0];
    this.props.exposeGamepad(gamepad);
    if (gamepad === undefined) return;

    let axes = gamepad.axes;
    let lerp = (x, y, alpha) => {
      return (1 - alpha) * x + alpha * y;
    };

    let speeds = axes
      .map((_, idx) => (idx % 2 === 0 ? 1 : -1) * Math.sign(axes[idx]))
      .map((val, idx) => (Math.abs(val * axes[idx]) < deadzone ? 0 : val))
      .map((val, idx) => val * lerp(0, 1, Math.abs(axes[idx])) ** this.state.gamma)
      .map((val, _) => val * this.state.maxSpeed);

    this.props.onMove(speeds[0], speeds[1], Math.atan2(speeds[1], speeds[0]));
    this.setState({
      leftJoystick: {
        left: speeds[0],
        top: speeds[1]
      },
      rightJoystick: {
        left: speeds[2],
        top: speeds[3]
      }
    });
  };

  axisChangeHandler = (axisName, value, previousValue) => {
    this.setState(state => {
      var leftJoystick = state.leftJoystick;

      if (Math.abs(leftJoystick.left) < 0.08) leftJoystick.left = 0;
      if (Math.abs(leftJoystick.top) < 0.08) leftJoystick.top = 0;

      this.props.onMove(
        leftJoystick.left,
        leftJoystick.top,
        Math.atan2(leftJoystick.top, leftJoystick.left)
      );
    });
  };

  render() {
    return (
      <Container>
        <Row className="mt-3">
          <Col>
            <p style={{ fontWeight: "bold" }}>
              {this.state.gamepad == null
                ? "No controller connected"
                : "Controller is connected"}
            </p>
            <p style={{ fontWeight: "bold", color: this.state["fingerStalled"] ? "red" : "green" }}>
              {this.state["fingerStalled"] ? "Stalling" : "Not Stalling"}
            </p>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <JoystickElement joy={this.state.leftJoystick} />
          </Col>
          <Col>
            <JoystickElement joy={this.state.rightJoystick} />
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <FormGroup>
              <Label for="gammaRange">
                Gamma: {this.state.gamma.toFixed(2)}
              </Label>
              <Input
                id="gammaRange"
                type="range"
                min="1"
                max="5"
                step="0.1"
                value={this.state.gamma}
                onChange={e => { this.setState({ gamma: +e.target.value }) }}
              />
              <Label for="maxSpeedRange">
                Max Speed: {(this.state.maxSpeed * 100).toFixed(0)}%
              </Label>
              <Input
                id="maxSpeedRange"
                type="range"
                min="1"
                max="100"
                value={this.state.maxSpeed * 100}
                onChange={e => { this.setState({ maxSpeed: (+e.target.value) / 100 }) }}
              />
              <Label for="drive-control-scheme">
                  Drive Control Mode:
              </Label>
              <ButtonGroup style={{ margin: 10, display: "flex" }}>
                <Button
                  color={
                    this.state.driveControlMode === DriveControlModes.dutyCycle ? "primary" : "secondary"
                  }
                  onClick={() => this.setDriveControlMode(DriveControlModes.dutyCycle)}
                  active={this.state.driveControlMode === DriveControlModes.dutyCycle}
                >
                  Duty Cycle
                </Button>
                <Button
                  color={
                    this.state.driveControlMode === DriveControlModes.velocity ? "primary" : "secondary"
                  }
                  onClick={() => this.setDriveControlMode(DriveControlModes.velocity)}
                  active={this.state.driveControlMode === DriveControlModes.velocity}
                >
                  Velocity
                </Button>
              </ButtonGroup>
              <Label for="drive-control-scheme">
                  Drive Control Scheme:
              </Label>
              <ButtonGroup style={{ margin: 10, display: "flex" }}>
                <Button
                  color={
                    this.state.driveControlScheme === DriveControlSchemes.singleStick ? "primary" : "secondary"
                  }
                  onClick={() => this.setDriveControlScheme(DriveControlSchemes.singleStick)}
                  active={this.state.driveControlScheme === DriveControlSchemes.singleStick}
                >
                  Single Stick
                </Button>
                <Button
                  color={
                    this.state.driveControlScheme === DriveControlSchemes.splitStick ? "primary" : "secondary"
                  }
                  onClick={() => this.setDriveControlScheme(DriveControlSchemes.splitStick)}
                  active={this.state.driveControlScheme === DriveControlSchemes.splitStick}
                >
                  Split Stick
                </Button>
                <Button
                  color={
                    this.state.driveControlScheme === DriveControlSchemes.tank ? "primary" : "secondary"
                  }
                  onClick={() => this.setDriveControlScheme(DriveControlSchemes.tank)}
                  active={this.state.driveControlScheme === DriveControlSchemes.tank}
                >
                  Tank
                </Button>
                <Button
                  color={
                    this.state.driveControlScheme === DriveControlSchemes.curve ? "primary" : "secondary"
                  }
                  onClick={() => this.setDriveControlScheme(DriveControlSchemes.curve)}
                  active={this.state.driveControlScheme === DriveControlSchemes.curve}
                >
                  Curve
                </Button>
              </ButtonGroup>
            </FormGroup>
          </Col>
        </Row>
      </Container>
    );
  }
}
