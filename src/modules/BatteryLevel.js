import React from "react";
import { Progress } from "reactstrap";
import { registerToSubscriber } from "../RosTopics";

export default class BatteryLevel extends React.Component {
  state = {
    batteryLevel: [[0, 0, 0], [0, 0, 0]]
  }

  // 5 percent increments
  voltagePercents = [
    3.27, 3.61, 3.69, 3.71, 3.73, 3.75, 3.77, 3.79, 3.8, 3.82, 3.84, 3.85, 3.87,
    3.91, 3.95, 3.98, 4.01, 4.08, 4.11, 4.15, 4.2,
  ];

  getBatteryPercentage(voltage) {
    let before, after, i;
    if (voltage >= this.voltagePercents[this.voltagePercents.length - 1]) {
      return 100;
    }

    if (voltage <= this.voltagePercents[0]) {
      return 0;
    }

    for (i = 0; i < this.voltagePercents.length; i++) {
      if (voltage < this.voltagePercents[i]) {
        before = this.voltagePercents[i - 1];
        after = this.voltagePercents[i];
        break;
      }
    }
    let lerp = (voltage - before) / (after - before);
    return Math.round((i + lerp) * 5);
  }

  componentDidMount() {
    registerToSubscriber("/battery_level",
      (msg) => this.setState({ batteryLevel: [msg.data.slice(0, 3), msg.data.slice(3, 6)] }));
  }

  render() {
    return (
      <div>
        {
          this.state.batteryLevel.map((battery, index) => {
            return (
              <div
                style={{
                  margin: 10,
                }}
                key={index}
              >
                <div>
                  <div style={{ margin: 10 }}>
                    Total —{" "}
                    {Math.round(battery.reduce((a, b) => a + b, 0) * 100) / 100} V
                  </div>
                  {battery.map((cell, index) => {
                    let percentage = this.getBatteryPercentage(cell);
                    return (
                      <Progress
                        animated
                        color={
                          percentage > 50
                            ? "success"
                            : percentage > 15
                              ? "warning"
                              : "danger"
                        }
                        style={{
                          height: 25,
                          fontSize: 15,
                          margin: 5,
                          borderRadius: 25 / 2,
                        }}
                        value={percentage + 20}
                        max={120}
                        key={index}
                      >
                        Cell {index + 1} — {Math.round(cell * 100) / 100} V (
                        {percentage}%)
                      </Progress>
                    );
                  })}
                </div>
              </div>
            );
          })
        }
      </div >
    );
  }
}
