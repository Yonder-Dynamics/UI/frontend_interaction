import React from 'react';
import { Stage, Layer, Line, Circle, Text } from 'react-konva';
import { registerToSubscriber } from '../RosTopics.js';

export default class ArmVisualization extends React.Component {
    state = {
        joint_states: [],
        shoulder_cartesian: [],
        bicep_cartesian: [],
        end_effector_cartesian: [],
    };

    componentDidMount() {
        const initial = [0, Math.PI / 4, Math.PI / 4, 0, 0]
        const [bicep_cartesian, end_effector_cartesian] = this.side_profile_cartesian(initial)
        const shoulder_cartesian = this.top_profile_cartesian(initial);
        this.setState({
            shoulder_cartesian: shoulder_cartesian,
            bicep_cartesian: bicep_cartesian,
            end_effector_cartesian: end_effector_cartesian
        });

        registerToSubscriber("/joint_states", (msg) => {
            const joint_states = msg["position"];
            const shoulder_cartesian = this.top_profile_cartesian(joint_states);
            const [bicep_cartesian, end_effector_cartesian] = this.side_profile_cartesian(joint_states)
            this.setState({ joint_states, shoulder_cartesian, bicep_cartesian, end_effector_cartesian });
        });
    }

    side_profile_cartesian(joint_states) {
        const L_a = 0.4064;  // length of bicep
        const L_e = 0.4385;  // length of forearm
        const theta_a = joint_states[1], theta_e = joint_states[2];
        const theta = theta_a + theta_e;

        const bicep_cartesian = [
            L_a * Math.sin(theta_a),
            L_a * Math.cos(theta_a),
        ];
        const end_effector_cartesian = [
            L_a * Math.sin(theta_a) + L_e * Math.sin(theta),
            L_a * Math.cos(theta_a) + L_e * Math.cos(theta),
        ];
        return [bicep_cartesian, end_effector_cartesian]
    }

    top_profile_cartesian(joint_states) {
        const L_a = 0.4064;  // length of bicep
        const L_e = 0.4385;  // length of forearm
        const L_t = L_a + L_e;
        const theta_s = joint_states[0];
        const shoulder_cartesian = [
            L_t * Math.sin(theta_s),
            L_t * Math.cos(theta_s),
        ];
        return shoulder_cartesian;
    }

    render() {
        let lineWidth = 200;
        let initialX = 100;
        let initialY = 200;
        let secondX = initialX + lineWidth * 1.5;
        let strokeWidth = 10;
        return (
            <Stage width={500} height={400}>
                <Layer>
                    <Line
                        x={initialX}
                        y={initialY}
                        points={[0, 0, this.state.bicep_cartesian[0] * lineWidth, -this.state.bicep_cartesian[1] * lineWidth,
                            this.state.end_effector_cartesian[0] * lineWidth, -this.state.end_effector_cartesian[1] * lineWidth]}
                        stroke="black"
                        strokeWidth={strokeWidth}
                    />
                    <Circle
                        x={initialX + this.state.bicep_cartesian[0] * lineWidth}
                        y={initialY - this.state.bicep_cartesian[1] * lineWidth}
                        radius={strokeWidth}
                        fill="grey"
                    />
                    <Circle
                        x={initialX}
                        y={initialY}
                        radius={strokeWidth * 1.5}
                        fill="grey"
                    />
                    <Circle
                        x={initialX + this.state.end_effector_cartesian[0] * lineWidth}
                        y={initialY - this.state.end_effector_cartesian[1] * lineWidth}
                        radius={strokeWidth}
                        fill="grey"
                    />
                    <Line
                        x={secondX}
                        y={initialY}
                        points={[0, 0, this.state.shoulder_cartesian[0] * lineWidth / 2, -this.state.shoulder_cartesian[1] * lineWidth / 2]}
                        stroke="black"
                        strokeWidth={strokeWidth}
                    />
                    <Circle
                        x={secondX}
                        y={initialY}
                        radius={strokeWidth * 1.5}
                        fill="grey"
                    />
                    <Circle
                        x={secondX + this.state.shoulder_cartesian[0] * lineWidth / 2}
                        y={initialY - this.state.shoulder_cartesian[1] * lineWidth / 2}
                        radius={strokeWidth}
                        fill="grey"
                    />
                    <Line
                        x={initialX + lineWidth}
                        y={0}
                        points={[0, 0, 0, 1000]}
                        stroke="black"
                    />
                    <Text
                        x={initialX}
                        y={350}
                        text="Side Profile" fontSize={15}
                    />
                    <Text
                        x={secondX}
                        y={350}
                        text="Top Profile" fontSize={15}
                    />
                </Layer>
            </Stage>
        );
    }
}

