import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  Dropdown,
  DropdownItem,
  Input,
  Label,
  Progress
} from "reactstrap";

import Popup from 'reactjs-popup';
import range from "lodash/range";

import { Stage, Layer, Text, Circle } from 'react-konva';
import { registerToSubscriber, writeToPublisher } from "../RosTopics.js";

const experimentToNum = {
  "Raman": 0,
  "Dapi": 1,
  "Nile Red": 2,
  "pH": 3,
  "Dark Background": 4,
  "Light Background": 5,
}

const defaultSettings = {
  "pH": [50, 10],
  "Nile Red": [250, 10],
  "Dapi": [250, 10],
  "Raman": [1000, 25],
}

function get_color(sample, exp, status) {
  let expStatus = status[sample][exp];
  if (expStatus === 0) return "white";
  if (expStatus === 1) return "red";
  if (expStatus === 2) return "yellow";
  if (expStatus === 3) return "green";
}

export default function ScienceControl() {
  const [isOpenSoil, setIsOpenSoil] = useState(false);
  const [isOpenTest, setIsOpenTest] = useState(false);
  const [soilSampleSelected, setSoilSample] = useState(0);
  const [expStatus, setExpStatus] = useState(Array(6).fill().map(() => Array(4).fill(0)));
  const [lastExpr, setLastExpr] = useState(null);
  const [testingInProgress, setTestingProg] = useState(false);
  const [soilLoading, setLoadingProg] = useState(false);
  const [testTypeSelected, setTestType] = useState("");
  const [integrationTime, setIntegrationTime] = useState(0);
  const [numAverages, setNumAverages] = useState(0);
  const [exprProgress, setExprProgress] = useState([0, 1])

  useEffect(() => {
    registerToSubscriber("/spectrometer_data",
      (msg) => {
        setTestingProg(false);
        setLastExpr([msg.data[0], msg.data[1]])
        return msg;
      });

    registerToSubscriber("/dump_soil_done",
      (msg) => setLoadingProg(false));

    registerToSubscriber("/experiment_progress",
      (msg) => setExprProgress(msg.data));
  }, []);

  useEffect(() => {
    if (lastExpr === null) return;
    let temp = [...expStatus];
    temp[lastExpr[0]][lastExpr[1]] = 3;
    setExpStatus(temp);
  }, [lastExpr])

  //Returns a String of current process in Science Box
  function testSampleStatus() {
    if (soilLoading)
      return "Soil is being Loaded into Carousel";
    else if (testingInProgress)
      return "Test is in Progress";
    // else if (expStatus[soilSampleSelected - 1][0] === 0)
    //   return "No soil in cubby " + soilSampleSelected;
    else if (!soilLoading && !testingInProgress)
      return "Ready To Run Test";
  }

  function setDefaultSettings(testType) {
    setTestType(testType);
    if (testType in defaultSettings) {
      setIntegrationTime(defaultSettings[testType][0]);
      setNumAverages(defaultSettings[testType][1]);
    } else {
      setIntegrationTime(0);
      setNumAverages(1);
    }
  }

  return (
    <Container>
      <Row className="mt-3">
        <Col>
          <Dropdown
            isOpen={isOpenSoil}
            toggle={() => setIsOpenSoil(!isOpenSoil)}
          >
            {soilSampleSelected === 0 ?
              <DropdownToggle caret> {'Select a Soil Sample : '} </DropdownToggle>
              :
              <DropdownToggle caret> {'Sample Selected : '} {soilSampleSelected}</DropdownToggle>
            }

            <DropdownMenu>
              {range(0, 6).map((i) => {
                return <DropdownItem onClick={() => {
                  setSoilSample(i + 1);
                }}> Soil Sample: {i + 1} </DropdownItem>
              })}
            </DropdownMenu>
          </Dropdown>

          <ButtonDropdown
            style={{ marginTop: 10 }}
            isOpen={isOpenTest}
            toggle={() => setIsOpenTest(!isOpenTest)}
          >
            {testTypeSelected === "" ?
              <DropdownToggle caret> {'Select a Test Type : '} </DropdownToggle>
              :
              <DropdownToggle caret> {'Test Type : '} {testTypeSelected}</DropdownToggle>
            }


            <DropdownMenu>
              {soilSampleSelected !== 0 ?
                <>
                  <DropdownItem onClick={() => setDefaultSettings("Raman")}> Raman </DropdownItem>
                  <DropdownItem onClick={() => setDefaultSettings("Dapi")}> Dapi </DropdownItem>
                  <DropdownItem onClick={() => setDefaultSettings("Nile Red")}> Nile Red </DropdownItem>
                  <DropdownItem onClick={() => setDefaultSettings("pH")}> pH </DropdownItem>
                  <DropdownItem onClick={() => setDefaultSettings("Dark Background")}> Dark Background </DropdownItem>
                  <DropdownItem onClick={() => setDefaultSettings("Light Background")}> Light Background </DropdownItem>
                </>
                :
                <>
                  <DropdownItem disabled> Raman </DropdownItem>
                  <DropdownItem disabled> Dapi </DropdownItem>
                  <DropdownItem disabled> Nile Red </DropdownItem>
                  <DropdownItem disabled> pH </DropdownItem>
                  <DropdownItem disabled> Dark Background </DropdownItem>
                  <DropdownItem disabled> Light Background </DropdownItem>
                </>
              }

            </DropdownMenu>
          </ButtonDropdown>
        </Col>

        <Col>
          <Label>Integration Time</Label>
          <Input type="text" value={integrationTime} onChange={e => setIntegrationTime(e.target.value)} />
        </Col>
        <Col>
          <Label># of Averages</Label>
          <Input type="text" value={numAverages} onChange={e => setNumAverages(e.target.value)} />
        </Col>

        <Col>
          {(soilSampleSelected !== 0 && !(testTypeSelected === "")) ?
            <>{testSampleStatus() === "Ready To Run Test" ?
              <Row>
                <Col>
                  <Button
                    outline
                    color="primary"
                    onClick={() => {
                      let exprNum = experimentToNum[testTypeSelected];
                      let toSend = [soilSampleSelected - 1, exprNum, parseInt(integrationTime), parseInt(numAverages)];
                      let temp = [...expStatus]

                      if (testTypeSelected !== "Dark Background" && testTypeSelected !== "Light Background") {
                        temp[soilSampleSelected - 1][exprNum] = 2;
                      }

                      setExpStatus(temp);
                      setTestingProg(true);
                      writeToPublisher("/run_experiment", { layout: { dim: [], data_offset: 0 }, data: toSend });
                    }}>
                    Test Sample
                  </Button>
                </Col>
              </Row>
              :
              <Row>
                <Col>
                  <Popup trigger={<div><Button style={{ backgroundColor: "red" }}> Actions Blocked </Button></div>} position="top"> <div> Warning: {testSampleStatus()} </div> </Popup>
                  <Button onClick={() => { setTestingProg(false); setLoadingProg(false); }} style={{ marginTop: 10 }}> OVERRIDE BLOCK </Button>
                </Col>
              </Row>
            }
            </>
            :
            <Button disabled> Select Valid Inputs to Test </Button>
          }
          <Button
            style={{ margin: 10, }}
            outline
            color="primary"
            onClick={() => {
              let temp = [...expStatus]
              temp[soilSampleSelected - 1] = Array(4).fill(1);
              setExpStatus(temp);
              setLoadingProg(true);
              writeToPublisher("/dump_soil", { data: soilSampleSelected - 1 });
            }}
          >
            Drop Soil
          </Button>
        </Col>
      </Row>

      <Row className="mt-3">
        <Col>
          <Stage width={420} height={420} style={{ paddingLeft: 20 }}>
            <Layer>
              <Circle x={200} y={200} radius={180} stroke={10} />

            </Layer>
            <Layer>
              {
                range(0, 6).map((i) => {
                  let temp = (i / 3) * Math.PI;
                  let c = 0.075
                  return <React.Fragment>
                    <Text x={200 + 200 * Math.cos(temp)} y={200 + 200 * Math.sin(temp)} text={i + 1} fontSize={15} />
                    <Circle x={200 + 150 * Math.cos(temp - c * 3)} y={200 + 150 * Math.sin(temp - c * 3)} radius={10} fill={get_color(i, 0, expStatus)} stroke={10} />
                    <Circle x={200 + 150 * Math.cos(temp - c)} y={200 + 150 * Math.sin(temp - c)} radius={10} fill={get_color(i, 1, expStatus)} stroke={10} />
                    <Circle x={200 + 150 * Math.cos(temp + c)} y={200 + 150 * Math.sin(temp + c)} radius={10} fill={get_color(i, 2, expStatus)} stroke={10} />
                    <Circle x={200 + 150 * Math.cos(temp + c * 3)} y={200 + 150 * Math.sin(temp + c * 3)} radius={10} fill={get_color(i, 3, expStatus)} stroke={10} />
                  </React.Fragment>

                })
              }
            </Layer>
          </Stage>
        </Col>

        {testingInProgress ? <Col> Experiment Progress: {exprProgress[0]} / {exprProgress[1]}<Progress
          animated
          color="success"
          style={{
            height: 50,
            fontSize: 15,
            margin: 5,
            borderRadius: 25 / 2,
          }}
          value={exprProgress[0]}
          max={exprProgress[1]}
        >
        </Progress></Col> : null}

      </Row>
    </Container >
  );
}
