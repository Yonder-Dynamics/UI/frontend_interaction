import React, { useState, useEffect } from "react";
import { Input, } from "reactstrap";

export const CoordInputMode = {
  DMS: "DMS",
  DEC: "DEC",
}

export const coordInputModeToString = (mode) => {
  switch (mode) {
    case CoordInputMode.DMS:
      return "Degrees Minutes Seconds";
    case CoordInputMode.DEC:
      return "Decimal";
    default:
      return "unknown mode" + mode;
  }
}

const dmsToDec = ({degrees, minutes, seconds}) => {
  const degreesSign = Math.sign(degrees)
  return degreesSign * (Math.abs(Number(degrees)) + Number(minutes) / 60 + Number(seconds) / 3600);
}

const decToDMS = (dec) => {
  dec = Number(dec);
  const decSign = Math.sign(dec);
  dec = Math.abs(dec)
  const degrees = Math.floor(dec);
  const minutes = Math.floor((dec - degrees) * 60);
  const seconds = (dec - degrees - minutes / 60) * 3600;
  return {degrees: (decSign * degrees), minutes, seconds};
}

const InputCoord = ({onCoordChange = () => {}, startCoord = "", mode = CoordInputMode.DEC}) => {
  const [ coordDEC, setCoordDEC ] = useState(startCoord);
  const [ coordDMS, setCoordDMS ] = useState(decToDMS(startCoord));

  useEffect(() => {
    switch (mode) {
      case CoordInputMode.DMS:
        setCoordDMS(decToDMS(coordDEC));
        break;
      case CoordInputMode.DEC:
        setCoordDEC(dmsToDec(coordDMS));
        break;
      default:
        console.error("unknown mode" + mode);
    }
  }, [mode]);

  useEffect(() => {
    setCoordDEC(startCoord);
    setCoordDMS(decToDMS(Number(startCoord)));
  }, [startCoord]);

  useEffect(() => {
    onCoordChange(coordDEC);
  }, [coordDEC]); 

  useEffect(() => {
    onCoordChange(dmsToDec(coordDMS));
  }, [coordDMS]); 

  const handleFocus = (event) => event.target.select();

  let ret;
  switch (mode) {
    case CoordInputMode.DMS:
      ret = (
      <div>
        Degrees:
        <Input
          value={coordDMS.degrees} 
          onChange={e => setCoordDMS({...coordDMS, degrees: e.target.value})}
          onFocus={handleFocus}
        />
        Minutes:
        <Input
          value={coordDMS.minutes} 
          onChange={e => setCoordDMS({...coordDMS, minutes: e.target.value})}
          onFocus={handleFocus}
        />
        Seconds:
        <Input
          value={coordDMS.seconds} 
          onChange={e => setCoordDMS({...coordDMS, seconds: e.target.value})}
          onFocus={handleFocus}
        />
      </div>
      );
      break;
    case CoordInputMode.DEC:
      ret = (
      <div>
        <Input 
          value={coordDEC} 
          onChange={e => setCoordDEC(e.target.value)} 
          onFocus={handleFocus}
        />
      </div>
      );
      break;
    default:
    ret = (
    <div>
      unknown mode: {mode}
    </div>
    )
  }

  return ret;
}

export default InputCoord;
