import React from "react";
import { Container, Col, Input, FormGroup, Form, Label, Button, ButtonGroup, Table } from "reactstrap";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import LeafletMap from "../utils/LeafletMap.js";
import InputCoord, { CoordInputMode, coordInputModeToString } from "./InputCoord.js";

import { writeToPublisher, registerToSubscriber } from "../RosTopics.js";
import PathHistoryTracker from "./PathHistoryTracker.js";

const waypointColors = ["red", "orange", "lime", "yellow", "blue", "aqua", "fuchsia"];
const waypointFields = ["lat", "lng", "name", "color", "radius"];

export default class GPSInput extends React.Component {
  state = {
    coordInputMode: CoordInputMode.DEC,
    latInput: "",
    longInput: "",
    latFromMap: "",
    longFromMap: "",
    nameInput: "",
    radiusInput: "",
    should_look_for_ar: false,
    id: "0",
    speed: "50",
    waypointList: [],
    editWaypointIdx: null,
    editWaypointValue: null,
    colorChosen: "red",
    togglePathTrack: false,
    pathList: [],
  };

  componentDidMount() {
    registerToSubscriber("/waypoint_list/get", (msg) => {
      this.setState({ waypointList: msg.waypoints });
    });

    // ros needs time to initialize 
    setTimeout(() => this.getWaypoints(), 2000);
  }

  setLatLong = (lat, long) => {
    this.setState({ latFromMap: lat, longFromMap: long });
  };

  submit = () => {
    let arLook = this.state.should_look_for_ar ? 1 : 0;
    let dataSend = [
      parseFloat(this.state.latInput),
      parseFloat(this.state.longInput),
      arLook,
      parseFloat(this.state.id),
      parseFloat(this.state.speed)];
    writeToPublisher("/rover_waypoint", { layout: { dim: [], data_offset: 0 }, data: dataSend });
  };

  publishWaypoints = () => {
    let msg = { waypoints: [] };
    msg.waypoints = this.state.waypointList;
    writeToPublisher("/waypoint_list/put", msg);
  }

  addWaypoint = () => {
    if (!isNaN(parseFloat(this.state.latInput)) && !isNaN(parseFloat(this.state.longInput))) {
      this.setState({
        waypointList: [...this.state.waypointList,
        {
          lat: this.state.latInput,
          lng: this.state.longInput,
          name: this.state.nameInput,
          color: this.state.colorChosen,
          radius: isNaN(parseFloat(this.state.radiusInput)) ? 0 : parseFloat(this.state.radiusInput)
        }
        ]
      }, () => { this.publishWaypoints() });
    }
  };

  getWaypoints = () => {
    writeToPublisher("/waypoint_list/request", { data: 0 });
  };

  removeWaypoint = (idx) => {
    let removed = [...this.state.waypointList.slice(0, idx), ...this.state.waypointList.slice(idx + 1)];
    this.setState({ waypointList: removed }, () => {
      this.publishWaypoints();
    });
  };

  /**
   * Remove path from state. 
   * Turn off path tracking so that you don't 
   * add new points to the path you just deleted.
   * 
   * @param { number } idx index of path within path list 
   */
  removePath = (idx) => {
    let removed = [...this.state.pathList.slice(0, idx), ...this.state.pathList.slice(idx + 1)];
    this.setState({ pathList: removed });
    this.setState({ togglePathTrack: false })
  }


  /**
   * Sets path tracking on or off
   * @param { boolean } newValue boolean if path tracking is on or off
   */
  updateTogglePathTrack = (newValue) => {
    this.setState({ togglePathTrack: newValue });
  }

  /**
   * Adds a new path to the pathList array
   * @param { Path } newValue - new path object
   */
  updatePathList = (newValue) => {
    this.setState({ pathList: [...this.state.pathList, newValue] });
  }

  /** 
   * Updates newest path in the pathList with new incoming 
   * position (lat, long) point. Creates copy of the pathList array, 
   * creates copy of the latest path obj in it, appends newest point to this 
   * path object's pathHistory field, updates last obj in our copy of 
   * the pathList array, and returns updated pathList array. 
   * 
   * @param {[number, number]} newValue - newest (lat, long) position
  */
  updateLatestPosition = (newValue) => {
    this.setState((prevState) => {
      const updatedPathList = [...prevState.pathList];

      // Only append new position if there is at least one path in list
      if (updatedPathList.length > 0) {
        const latestPath = { ...updatedPathList[updatedPathList.length - 1] };
        latestPath.pathHistory = [...latestPath.pathHistory, newValue];
        updatedPathList[updatedPathList.length - 1] = latestPath;
      }
      return { pathList: updatedPathList };
    });
  }


  render() {
    return (
      <div>
        <Container style={{ padding: 20 }}>
          <LeafletMap
            setLatLong={this.setLatLong}
            waypoints={this.state.waypointList}
            pathList={this.state.pathList}
            togglePathTrack={this.state.togglePathTrack} />

          <Form style={{ padding: 20, paddingBottom: 0 }}>
            <Label>Coordinate Input Mode:</Label>
            <UncontrolledDropdown>
              <DropdownToggle caret>
                {coordInputModeToString(this.state.coordInputMode)}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem
                  onClick={() => { this.setState({ coordInputMode: CoordInputMode.DMS }) }}>
                  {coordInputModeToString(CoordInputMode.DMS)}
                </DropdownItem>
                <DropdownItem
                  onClick={() => { this.setState({ coordInputMode: CoordInputMode.DEC }) }}>
                  {coordInputModeToString(CoordInputMode.DEC)}
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <FormGroup row>
              <Col>
                <b>Latitude</b>
                <InputCoord
                  mode={this.state.coordInputMode}
                  onCoordChange={(coord) => this.setState({ latInput: coord })}
                  startCoord={this.state.latFromMap}
                />
                <b>Longitude</b>
                <InputCoord
                  mode={this.state.coordInputMode}
                  onCoordChange={(coord) => this.setState({ longInput: coord })}
                  startCoord={this.state.longFromMap}
                />
              </Col>
              <Col>
                <Label>Name</Label>
                <Input type="text" value={this.state.nameInput} onChange={e => this.setState({ nameInput: e.target.value })} />
                <Label>Color</Label>
                <UncontrolledDropdown>
                  <DropdownToggle caret>
                    {this.state.colorChosen}
                  </DropdownToggle>
                  <DropdownMenu>
                    {waypointColors.map((value, idx) => {
                      return <DropdownItem key={idx} onClick={() => { this.setState({ colorChosen: value }) }}>{value}</DropdownItem>
                    })
                    }
                  </DropdownMenu>
                </UncontrolledDropdown>
                <Label>Radius</Label>
                <InputCoord
                  onCoordChange={(coord) => this.setState({ radiusInput: coord })}
                />

              </Col>
              <Label style={{ paddingRight: 20 }}>Look for AR tag</Label>
              <Col><Input type="checkbox" onChange={e => this.setState({ should_look_for_ar: e.target.checked })} /></Col>

              <Label>ID</Label>
              <Col><Input type="text" value={this.state.id} onChange={e => this.setState({ id: e.target.value })} /></Col>

              <Label>Speed</Label>
              <Col><Input type="text" value={this.state.speed} onChange={e => this.setState({ speed: e.target.value })} /></Col>
              <ButtonGroup vertical>
                <Button onClick={this.submit}>Submit for Auton</Button>
                <Button onClick={this.addWaypoint} style={{ marginTop: 10 }}>Add to Map</Button>
                <Button onClick={this.getWaypoints}>Update Waypoints</Button>
              </ButtonGroup >

            </FormGroup >
          </Form >


          <p>Click on a table entry to edit, then click Enter to save.</p>
          <Table striped hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Name</th>
                <th>Icon Color</th>
                <th>Radius</th>
                <th>Remove</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.waypointList.map((val, idx) => {
                  return (
                    <tr key={idx}>
                      <th scope="row">{idx + 1}</th>

                      {
                        waypointFields.map((field) => {
                          return (
                            <td onClick={() => { this.setState({ editWaypointIdx: [idx, field], editWaypointValue: this.state.waypointList[idx][field] }) }}>
                              {this.state.editWaypointIdx !== null && this.state.editWaypointIdx[0] == idx && this.state.editWaypointIdx[1] == field
                                ? <Input type="text" value={this.state.editWaypointValue}
                                  onChange={e => this.setState({ editWaypointValue: e.target.value })}
                                  onKeyDown={e => {
                                    if (e.key === "Enter") {
                                      let toChangeIdx = this.state.editWaypointIdx[0];
                                      let temp = JSON.parse(JSON.stringify(this.state.waypointList));
                                      let changeVal = this.state.editWaypointValue;
                                      if (field == "lat" || field == "lng") changeVal = parseFloat(changeVal);
                                      temp[toChangeIdx][this.state.editWaypointIdx[1]] = changeVal;
                                      this.setState({ editWaypointIdx: null, editWaypointValue: null, waypointList: temp }, () => {
                                        this.publishWaypoints();
                                      });

                                    }
                                  }
                                  }
                                /> : val[field]}
                            </td>)
                        })
                      }

                      <td onClick={() => { this.removeWaypoint(idx) }}>x</td>
                    </tr>)
                })
              }
            </tbody >
          </Table >


          <Label>Path History Input Mode:</Label>
          <PathHistoryTracker
            togglePathTrack={this.state.togglePathTrack}
            updateTogglePathTrack={this.updateTogglePathTrack}

            pathList={this.state.pathList}
            updatePathList={this.updatePathList}
            updateLatestPosition={this.updateLatestPosition}
          />

          <Table striped hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Latest Latitude</th>
                <th>Latest Longitude</th>
                <th>Path Color</th>
                <th>Remove</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.pathList && this.state.pathList.map((val, idx) => {
                  if (val.pathHistory.length > 0) {
                    return (
                      <tr key={idx}>
                        <th scope="row">{idx + 1}</th>
                        <td>{val.pathHistory[val.pathHistory.length - 1][0]}</td>
                        <td>{val.pathHistory[val.pathHistory.length - 1][1]}</td>
                        <td>{val.color}</td>
                        <td onClick={() => { this.removePath(idx) }}>x</td>
                      </tr>)
                  }
                })
              }
            </tbody >
          </Table >


        </Container >
      </div >
    );
  }
}
