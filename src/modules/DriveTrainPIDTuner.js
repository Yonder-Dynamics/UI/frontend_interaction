import React from "react";

import { writeToPublisher } from "../RosTopics";
import PIDFTuner from "./PIDFTuner.js";

const DriveTrainPIDTuner = (props) => {
    const sendDrivePID = (values) => {
        let dataSend = [
            parseFloat(values["P"]),
            parseFloat(values["I"]),
            parseFloat(values["D"]),
            parseFloat(values["F"]),
        ]

        writeToPublisher("/drive_train/set_pid", { layout: { dim: [], data_offset: 0 }, data: dataSend });
    }

    return (
              <PIDFTuner label={"Drive Train"} onSubmit={sendDrivePID}/>
    )
}

export default DriveTrainPIDTuner
