import React, { useState, useEffect, useRef } from "react";
import Janus from "janus-gateway-js";
import {
  Button,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";

import { Stage, Layer, Line, Circle, Text } from 'react-konva';
import CameraCompass from './CameraCompass';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export default function Livestream() {
  const [streaming, setStreaming] = useState();
  const [streams, setStreams] = useState();
  const [selectedStreamId, setSelectedStreamId] = useState();
  const [streamStatus, setStreamStatus] = useState();
  const [activeStream, setActiveStream] = useState();

  const [size, setSize] = useState();
  const [ratio, setRatio] = useState();
  const [bitrate, setBitrate] = useState();

  const videoRef = useRef();
  const [isOpen, setIsOpen] = useState();

  const refreshStreamList = async () => {
    try {
      const response = await streaming.list();
      setStreams(response._plainMessage.plugindata.data.list);
    } catch { }
  };

  let prevBytesReceived;
  let prevTimestamp;
  useInterval(async () => {
    if (!activeStream) return;

    const connectionStats = await streaming.getPeerConnection().getStats();
    const videoStats = Array.from(connectionStats.values()).filter(
      (x) => x.type === "inbound-rtp"
    );
    if (!videoStats) return;

    const stats = videoStats[0];
    if (!prevBytesReceived || !prevTimestamp) {
      prevBytesReceived = stats.bytesReceived;
      prevTimestamp = stats.timestamp;
      return;
    }

    const diffBytesReceived = stats.bytesReceived - prevBytesReceived;
    const diffTimestamp = stats.timestamp - prevTimestamp;
    setBitrate(`${Math.round((diffBytesReceived * 8) / diffTimestamp)} Kb/s`);
  }, 1000);

  useEffect(() => {
    (async () => {
      const janus = new Janus.Client("ws://192.168.0.50:8188/", {
        keepalive: "true",
      });
      const connection = await janus.createConnection("client");
      const session = await connection.createSession();
      const streaming = await session.attachPlugin(Janus.StreamingPlugin.NAME);

      streaming.on("pc:track:remote", async (event) => {
        if (event.type === "track") {
          const stream = new MediaStream();
          stream.addTrack(event.track.clone());
          setActiveStream(stream);
          if (videoRef.current) {
            if (videoRef.current.srcObject)
              videoRef.current.srcObject.getTracks().forEach((x) => x.stop());
            videoRef.current.srcObject = stream;
            videoRef.current.addEventListener("loadedmetadata", () => {
              setSize(
                `${videoRef.current.videoWidth}×${videoRef.current.videoHeight}`
              );
              setRatio(videoRef.current.videoWidth / videoRef.current.videoHeight)
            }
            );
            await videoRef.current.play();
          }
        }
      });

      streaming.on("message", (message) => {
        if (message._plainMessage.janus === "event") {
          if (message._plainMessage.plugindata.data.result.switched === "ok")
            return;
          setStreamStatus(message._plainMessage.plugindata.data.result.status);
        }
      });

      setStreaming(streaming);
      setStreams((await streaming.list())._plainMessage.plugindata.data.list);
    })().catch((error) => { console.error("error in Livestream.js!: ", error) });
  }, []);
  console.log(ratio)
  return (
    <>
      <div class="outerstream">
        <div class="top">
          <CameraCompass ratio={ratio} />
        </div>
        <div class="below">
          <video
            ref={videoRef}
            autoPlay
            playsInline
            hidden={activeStream === undefined}
            style={{
              width: window.innerWidth * .4,
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
            }}
          />
        </div>
      </div>
      <div style={{ padding: 10, paddingBottom: 0 }}>
        <ButtonDropdown isOpen={isOpen} toggle={() => setIsOpen(!isOpen)}>
          <DropdownToggle caret>Select Stream</DropdownToggle>
          {streams ? (
            <DropdownMenu>
              {streams.map((x) => {
                return (
                  <DropdownItem
                    key={x.id}
                    active={x.id === selectedStreamId}
                    onClick={async () => {
                      setSelectedStreamId(x.id);
                      await streaming.connect(x.id);
                    }}
                  >
                    {x.id}: {x.description}
                  </DropdownItem>
                );
              })}
            </DropdownMenu>
          ) : undefined}
        </ButtonDropdown>
        <Button
          style={{ marginLeft: 10 }}
          outline
          color="primary"
          onClick={refreshStreamList}
        >
          Refresh Stream List
        </Button>
        {streamStatus ? (
          <div style={{ float: "right", flex: 1, flexDirection: "row" }}>
            <p style={{ marginRight: 10 }}>
              Stream {selectedStreamId} Status:{" "}
              <strong style={{ textTransform: "capitalize" }}>
                {streamStatus}
              </strong>
            </p>
            {size ? (
              <Button color="primary" size="sm">
                {size}
              </Button>
            ) : undefined}
            {bitrate ? (
              <Button color="info" size="sm" active style={{ marginLeft: 10 }}>
                {bitrate}
              </Button>
            ) : undefined}
          </div>
        ) : undefined}
      </div>
    </>
  );
}
