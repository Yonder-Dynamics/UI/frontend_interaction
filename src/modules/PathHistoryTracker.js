import React from "react";
import { Container, Col, Input, Form, Label, Button } from "reactstrap";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { registerToSubscriber } from "../RosTopics.js";

const pathColors = ["red", "orange", "lime", "yellow", "blue", "purple"];

export default class PathHistoryTracker extends React.Component {
    state = {
      globalPositionsList: [], 
      colorChosen: "red", 
      
      prevPointTime: Date.now(), 
      pathUpdateBuffer: 5
    };

    /**
     * Update position list and newest path with latest position if
     * path tracking is on and time delay has passed.
     * Note: Date object arithmetic performed in milliseconds. 
     */
    componentDidMount() {
      registerToSubscriber("/mavros/global_position/global", (msg) => {

        this.setState({
          globalPositionsList: [...this.state.globalPositionsList, [msg.latitude,  msg.longitude]]
        }); 

        const currentTime = Date.now(); 
        if (this.props.togglePathTrack == true && 
            currentTime - this.state.prevPointTime > 1000 * this.state.pathUpdateBuffer) {

          this.props.updateLatestPosition([msg.latitude,  msg.longitude]); 
          this.setState({prevPointTime: currentTime}); 

        }
      });
    }
    
    /**
     * Turn path tracking off/on. 
     * If you turn it on, create new path obj 
     * with color & path points array. 
     * Add this obj to PathList in GPSInput.js 
     */
    handlePathClick = () => {
      if (this.props.togglePathTrack == true) {
        this.props.updateTogglePathTrack(false); 
      }
      else {
        this.props.updatePathList({
          pathId: this.props.pathList.length,  
          color: this.state.colorChosen, 
          pathHistory: []
        }); 

        this.props.updateTogglePathTrack(true); 
      }
    }
  
    render() {
      return (
        <div>
          <Container style={{ padding: 20 }}>

            <Form style={{
              display: 'flex', 
              flexDirection: 'row', 
              alignItems: 'flex-end',
              justifyContent: 'flex-start'
            }}>
                <Col>
                  <Label>Path Color </Label>
                  <UncontrolledDropdown>
                    <DropdownToggle caret>
                      {this.state.colorChosen}
                    </DropdownToggle>
                    <DropdownMenu>
                      {pathColors.map((value, idx) => {
                        return <DropdownItem key={idx} onClick={() => { this.setState({ colorChosen: value }) }}>{value}</DropdownItem>
                      })
                      }
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Col>
                
                <Col>
                  <Label>Update Time Delay (sec)</Label>
                  <Input 
                    type="text" 
                    value={this.state.pathUpdateBuffer} 
                    onChange={e => this.setState({ pathUpdateBuffer: e.target.value })} 
                  />
                </Col>

                <Col>
                  <Button onClick={this.handlePathClick}>
                      {this.props.togglePathTrack ? (
                        <div>Path Tracking: ON</div>
                      ) : (
                        <div>Path Tracking: OFF</div>
                      )}
                  </Button>
                </Col>
            </Form>

          </Container >
        </div >
      );
    }
  }
  