import React from "react";

import "./App.css";
import { Alert, Row, Col } from "reactstrap";
import cardData from "./CardData";
import MakeCard from "./utils/Card.js";
import TopNavbar from "./modules/TopNavbar.js";
import { RosConnection } from "rosreact";
import RosTopics from "./RosTopics";

export default class App extends React.Component {
  state = {
    width: window.innerWidth,
    roverConnected: false,

    rosSubs: {},
    rosPubs: {},

  };

  constructor(props) {
    super(props);
    this.Joystick = React.createRef();
    this.KeyboardControl = React.createRef();
    this.RobotOrientation = React.createRef();
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentDidMount() {
    document.body.style.background = "#F4F4F4";
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  setRoverConnected = (connected) => {
    // TODO: We should do global state / figure out why this causes full app rerenders
    // this is a bandaid solution
    if (this.state.roverConnected !== connected) {
      this.setState({ roverConnected: connected });
    }
  }

  alert = () => {
    return this.state.roverConnected ? (
      <div />
    ) : (
      <Alert color="danger" style={{ paddingTop: 20 }}>
        Rover unreachable
      </Alert>
    );
  };

  render() {
    const ismobile = this.state.width <= 1000;
    const webbridgeURL = process.env.REACT_APP_ROS_WEBBRIDGE_URL;
    if (ismobile) {
      return (
        <RosConnection url={webbridgeURL} autoConnect>

          <RosTopics setRoverConnected={this.setRoverConnected} />
          <div
            tabIndex="1"
            onKeyDown={e => this.KeyboardControl.current.onKeyDown(e)}
            onKeyUp={e => this.KeyboardControl.current.onKeyUp(e)}
          >
            <TopNavbar />
            {this.alert()}
            <ul
              style={{ listStyleType: "none" }}
              className="stagger"
            >
              {cardData(this).rows.map((row, rowIndex) => {
                const rowKey = cardData(this)
                  .rows.slice(0, rowIndex)
                  .map(row => row.columns)
                  .reduce((total, num) => total + num, 0);
                return row.columns.map((card, cardIndex) => {
                  return (
                    // <MakeCard key={rowKey + cardIndex} body={card.body}>
                    //   {card.data}
                    // </MakeCard>
                    card["rows"] === undefined ?
                      <MakeCard body={card.body}>{card.data}</MakeCard>
                      :
                      card["rows"].map((multicard) => {
                        return <MakeCard body={multicard.body}>{multicard.data}</MakeCard>
                      })

                  );
                });
              })}
            </ul>
          </div>
        </RosConnection>
      );
    } else {
      return (
        <RosConnection url={webbridgeURL} autoConnect>
          <RosTopics setRoverConnected={this.setRoverConnected} />
          <div
            tabIndex="1"
            onKeyDown={e => this.KeyboardControl.current.onKeyDown(e)}
            onKeyUp={e => this.KeyboardControl.current.onKeyUp(e)}
          >
            <TopNavbar />
            {this.alert()}
            <div className="px-5">
              <ul
                style={{ listStyleType: "none" }}
                className="stagger"
              >
                {cardData(this).rows.map((row, rowIndex) => {
                  return (
                    <Row key={rowIndex} className={row.className}>
                      {row.columns.map((card, cardIndex) => {
                        return <Col key={cardIndex} className={card.className}>
                          {
                            card["rows"] === undefined ?
                              <MakeCard body={card.body}>{card.data}</MakeCard>
                              :
                              card["rows"].map((multicard) => {
                                return <Row ><MakeCard body={multicard.body}>{multicard.data}</MakeCard></Row>
                              })
                          }
                        </Col>
                      })}
                    </Row>
                  );
                })}
              </ul>
            </div>
          </div>
        </RosConnection >
      );
    }

  }

}
