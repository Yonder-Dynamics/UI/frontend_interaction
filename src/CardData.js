import React from "react";
import DrivetrainManual from "./modules/DrivetrainManual";
import JoystickReader from "./modules/JoystickReader";
import KeyboardControl, { keysHandled } from "./modules/KeyboardControl";
import Spectroscopy from "./modules/Spectroscopy";
import GPSInput from "./modules/GPSInput";
import BatteryLevel from "./modules/BatteryLevel";
import Livestream from "./modules/Livestream";
import ScienceControl from "./modules/ScienceControl";
import ArmVisualization from "./modules/ArmVisualization";

export default function cardData(thisObj) {
  return {
    rows: [
      {
        className: "my-4",
        columns: [
          {
            rows: [
              {
                body: {
                  title: "Control",
                },
                data: (
                  <DrivetrainManual
                    onMove={(x, y, rotation) => {
                      // for virtual joystick
                      let joystickReader = thisObj.Joystick.current;
                      let noJoy = joystickReader?.state.gamepad == null;
                      if (noJoy) {
                        joystickReader?.props?.onMove(x, y, rotation);
                      }
                    }}
                    setKeyboardControlState={(shouldSubmit) =>
                      thisObj.KeyboardControl.current.setState({ shouldSubmit })
                    }
                    setJoystickReaderState={(shouldSubmit) =>
                      thisObj.Joystick.current.setState({ shouldSubmit })
                    }
                  />

                ),
              },

              {
                body: {
                  title: "Keyboard Control",
                  text: "Valid Keys: " + keysHandled.join(", "),
                },
                data: <KeyboardControl
                  ref={thisObj.KeyboardControl}
                />,
              },


            ]

          },
          {
            className: "col-3",
            body: {
              title: "Joystick Feedback",
              text: "Reads input from controller",
            },
            data: (
              <JoystickReader
                ref={thisObj.Joystick}
                onMove={(x, y, rotation) => {
                  thisObj.Joystick.current.setState({
                    leftJoystick: {
                      top: y,
                      left: x,
                    },
                  });
                }}
                exposeGamepad={(gamepad) =>
                  thisObj.KeyboardControl.current.pollGamepad(gamepad)
                }
                driveControlMode={thisObj.state.driveControlMode}
              />
            ),
          },
          {
            rows: [
              {
                body: {
                  title: "Battery Level",
                },
                data: (
                  <BatteryLevel />

                ),
              },

              {
                body: {
                  title: "Arm Visualization",
                },
                data: <ArmVisualization />,
              },


            ]
          },
        ],
      },
      {
        className: "my-4",
        columns: [
          {
            body: {
              title: "Live Camera Feed",
            },
            data: <Livestream />,
          },
          {
            body: {
              title: "Secondary Camera Feed",
            },
            data: <Livestream />,
          },
        ],
      },
      {
        className: "my-4",
        columns: [
          {
            body: {
              title: "GPS Input",
            },
            data: <GPSInput />,
          },
        ],
      },
      {
        className: "my-4",
        columns: [
          {
            body: {
              title: "Science Control",
            },
            data: <ScienceControl />,
          },
        ],
      },
      {
        className: "my-4",
        columns: [
          {
            body: {
              title: "Raman Spectroscopy",
            },
            data: <Spectroscopy />,
          },
        ],
      },
    ],
  }
}

