import React from "react";
import { Card } from "reactstrap";

export default class RoundCard extends React.Component {
  render() {
    return (
      <li
        style={{
          margin: this.props.ismobile ? 20 : 0,
          marginTop: this.props.ismobile ? 0 : 20,
          marginRight: this.props.ismobile ? 0 : 20,
          marginLeft: 0,
          borderRadius: "10px",
          width: "100%",
          height: "100%"
        }}
      >

        <Card
          {...this.props}
          style={{
            borderWidth: 0,
            borderRadius: "10px"
          }}
        >
          {this.props.children}
        </Card>

      </li>
    );
  }
}
