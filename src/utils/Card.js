import React from "react";
import { CardBody } from "reactstrap";
import RoundCard from "./RoundCard";

function MakeCardBody(props) {
  return (
    <CardBody>
      {props.body.title ? <h1>{props.body.title}</h1> : null}
      {props.body.subtitle ? <h4>{props.body.subtitle}</h4> : null}
      {props.body.text ? <p>{props.body.text}</p> : null}
    </CardBody>
  );
}

function MakeCard(props) {
  return (
    <RoundCard ismobile={props.ismobile} state={props.state}>
      {props.children === undefined ? <div /> : props.children}
      {props.body === undefined ? <div /> : <MakeCardBody body={props.body} />}
    </RoundCard>
  );
}

export default MakeCard;
