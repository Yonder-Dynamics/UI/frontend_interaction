// https://stackoverflow.com/questions/66342168/how-to-dynamically-move-and-rotate-marker-in-react-leaflet
import React, { forwardRef, useRef, useEffect } from "react";
import { Marker } from 'react-leaflet';
import "leaflet-rotatedmarker"

const RotatedMarker = forwardRef(({ children, ...props }, forwardRef) => {
  const markerRef = useRef();

  const { rotationAngle, rotationOrigin } = props;
  useEffect(() => {
    const marker = markerRef.current;
    if (marker) {
      marker.setRotationAngle(rotationAngle);
      marker.setRotationOrigin(rotationOrigin);
    }
  }, [rotationAngle, rotationOrigin]);

  return (
    <Marker
      ref={(ref) => {
        markerRef.current = ref;
        if (forwardRef) {
          forwardRef.current = ref;
        }
      }}
      {...props}
    >
      {children}
    </Marker>
  );
});

export default RotatedMarker
