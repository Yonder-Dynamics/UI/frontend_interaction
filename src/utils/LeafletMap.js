import React, { useEffect, useState } from "react";
import { Marker, MapContainer, TileLayer, Popup, Tooltip, useMapEvent, Polyline, Circle } from 'react-leaflet';
import L from 'leaflet';
import RotatedMarker from './LeafletRotatedMarker.js'

import roverImage from "../images/rover_pointer.png"
import { registerToSubscriber } from "../RosTopics";

// hack since normal icons break when css file is loaded locally
L.Icon.Default.imagePath = 'leaflet/';
const iconSizePixels = 40;

const minZoom = 12;
const maxZoom = 17;

let roverIcon = L.icon({
    iconUrl: roverImage,
    iconSize: [iconSizePixels, iconSizePixels],
    iconAnchor: [Math.floor(iconSizePixels / 2), Math.floor(iconSizePixels / 2)],
})

let userWaypointIcon = (color) => L.divIcon({
    html: `
    <svg width="30" height="30" viewBox="-4 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g id="Vivid.JS" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <g id="Vivid-Icons" transform="translate(-125.000000, -643.000000)">
        <g id="Icons" transform="translate(37.000000, 169.000000)">
          <g id="map-marker" transform="translate(78.000000, 468.000000)">
            <g transform="translate(10.000000, 6.000000)">
              <path d="M14,0 C21.732,0 28,5.641 28,12.6 C28,23.963 14,36 14,36 C14,36 0,24.064 0,12.6 C0,5.641 6.268,0 14,0 Z" id="Shape" fill=${color}>

              </path>
              <circle id="Oval" fill="#fff" fill-rule="nonzero" cx="14" cy="14" r="7">

              </circle>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>`,
    className: "svg-icon",
    iconSize: [30, 30],
    iconAnchor: [15, 30],
})

function LocationMarker(props) {
    const [position, setPosition] = useState(null)
    const map = useMapEvent('click', (e) => {
        setPosition(e.latlng)
        props.setLatLong(e.latlng.lat, e.latlng.lng)
    })

    return position === null ? null : (
        <Marker position={position} />
    )
}

export default function LeafletMap(props) {
    // ucsd
    // const [center, setCenter] = useState({ lat: 32.8793, lng: -117.2322 });
    //  utah testing site
    // const [center, setCenter] = useState({ lat: 38.368244, lng: -110.913942 });
    // mdrs
    const [center, setCenter] = useState({ lat: 38.4064729, lng: -110.7916622 });
    const [zoom, setZoom] = useState(15);
    const [roverCoord, setRoverCoord] = useState(null);
    const [roverHeading, setRoverHeading] = useState(null);


    useEffect(() => {
        registerToSubscriber("/mavros/global_position/global",
            (msg) => setRoverCoord({ lat: msg.latitude, lng: msg.longitude }));
        registerToSubscriber("/mavros/global_position/compass_hdg",
            (msg) => setRoverHeading(msg.data)
        );
    }, [])

    const rotation = roverHeading ?? 0
    return (
        <div id="map-id">
            <div>
                <div style={{ width: "20rem" }}>Heading: {rotation} </div>

                <div> Position: lat: {roverCoord?.lat ?? 0} lon: {roverCoord?.lng ?? 0}</div>
            </div>
            <MapContainer
                center={center}
                zoom={zoom}
                minZoom={minZoom}
                style={{ height: '80vh', width: '100wh', borderRadius: '10px', overflow: 'hidden' }}
            >
                <TileLayer
                    url="./tiles_topo/{z}/{x}/{y}.png"
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    maxNativeZoom={maxZoom}
                    maxZoom={25}
                />
                <LocationMarker setLatLong={props.setLatLong} color={"red"} />

                {props.waypoints.map((waypoint, idx) => {
                    return <React.Fragment>
                        <Marker
                            position={{ lat: waypoint.lat, lng: waypoint.lng }}
                            icon={userWaypointIcon(waypoint.color)}
                            key={idx}
                        >
                            <Popup>
                                Latitude: {waypoint.lat} <br /> Longitude: {waypoint.lng} <br /> Name: {waypoint.name}
                            </Popup>
                            <Tooltip direction="right" offset={[0, 0]} opacity={1} permanent>{waypoint.name}</Tooltip>
                        </Marker>;
                        {waypoint.radius > 0 ? <Circle
                            center={{ lat: waypoint.lat, lng: waypoint.lng }}
                            fillOpacity={0}
                            color={waypoint.color}
                            radius={waypoint.radius} /> : null}
                    </React.Fragment>
                })}
                {roverCoord &&
                    <RotatedMarker
                        position={roverCoord}
                        icon={roverIcon}
                        rotationAngle={rotation}
                        rotationOrigin="center"
                    >
                    </RotatedMarker>}

                {props.togglePathTrack && props.pathList && props.pathList.map((path, idx) => (
                    <Polyline
                        key={idx}
                        pathOptions={{ color: path.color }}
                        positions={path.pathHistory}
                    />
                ))}
            </MapContainer>
        </div>
    );
}

